# Docker Testing Plugin

## Purpose

This plugin enables running docker containers to support testing JVM projects.

## Setup

This plugin requires docker (at least v1.11) to work properly. 

- On linux, all you need is to have docker installed and running. The plugin will default to communicating over the linux
socket file. 

- On macOS, Docker for Mac is the best choice - that will work without extra configuration because it uses the linux socket 
file. If this is not an option, DockerToolbox will work - however you must configure the DOCKER_* environment variables
for gradle to work properly.

- On Windows, Docker for Windows is the best choice, however you need to configure it to expose the daemon on 
tcp://localhost:2375, and configure the DOCKER_HOST environment variable[^1]. DockerToolbox will work, however you must 
configure the DOCKER_* environment variables for gradle to work properly.

[^1]: This is because Docker for Windows is now using named pipes to communicate with the daemon. These are similar to socket files in UNIX, however the backends for the java docker api do not yet support HTTP over named pipes on windows. Once this support is added, exposing the daemon and configuring DOCKER_HOST will no longer be needed.

## Examples

There are a couple examples using this plugin to test various types of projects (currently two of the three are configured
to use this plugin - db and s3). To run the tests, you just have to run the standard gradle test task (i.e. 
`./gradlew test` or `./gradlew build`) - this will create and run the containers, then tear them down after the tests 
are finished.

## Current implementation

The plugin adds two additional tasks which sandwich the built-in test task. 

1. `test` depends on `setupTestContainers`. This task first ensures that any previous run is cleaned up properly (i.e. 
it will remove any containers that weren't cleaned up previously, and destroy the network if it wasn't properly removed).
It will then create a network, and start all of the specified containers in this network with the appropriate alias. Once
all of the started containers are healthy (or just running if they don't have any built-in healthchecks), the task will 
finish.

2. `teardownTestContainers` finalizes `test`. This task destroys all of the created containers, then destroys the network
that was created.

A couple things of note:

* Occasionally gradle can get into a wonky state which requires stopping the daemons (using `./gradlew --stop`).

* Running tests for multiple projects in parallel should work correctly, but has not been thoroughly tested.

* Both tasks can be run independently if you want to see their effect. 

* The docker library will print out an error if the network doesn't exist. This exception is fine to ignore - this is 
printed when the setup/teardown task looks for the network and cannot find it. This is printed because the setup task 
tries to cleanup anything left by previous invocations if the teardown task failed. The teardown task will print this if
it can't find anything to teardown. It will look similar to:
```bash
Error during callback
com.github.dockerjava.api.exception.NotFoundException: {"message":"network :examples:s3 not found"}

        at com.github.dockerjava.netty.handler.HttpResponseHandler.channelRead0(HttpResponseHandler.java:103)
        at com.github.dockerjava.netty.handler.HttpResponseHandler.channelRead0(HttpResponseHandler.java:33)
        at io.netty.channel.SimpleChannelInboundHandler.channelRead(SimpleChannelInboundHandler.java:105)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.handler.logging.LoggingHandler.channelRead(LoggingHandler.java:241)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.channel.CombinedChannelDuplexHandler$DelegatingChannelHandlerContext.fireChannelRead(CombinedChannelDuplexHandler.java:438)
        at io.netty.handler.codec.ByteToMessageDecoder.fireChannelRead(ByteToMessageDecoder.java:310)
        at io.netty.handler.codec.ByteToMessageDecoder.channelRead(ByteToMessageDecoder.java:284)
        at io.netty.channel.CombinedChannelDuplexHandler.channelRead(CombinedChannelDuplexHandler.java:253)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.AbstractChannelHandlerContext.fireChannelRead(AbstractChannelHandlerContext.java:340)
        at io.netty.channel.DefaultChannelPipeline$HeadContext.channelRead(DefaultChannelPipeline.java:1334)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:362)
        at io.netty.channel.AbstractChannelHandlerContext.invokeChannelRead(AbstractChannelHandlerContext.java:348)
        at io.netty.channel.DefaultChannelPipeline.fireChannelRead(DefaultChannelPipeline.java:926)
        at io.netty.channel.nio.AbstractNioByteChannel$NioByteUnsafe.read(AbstractNioByteChannel.java:134)
        at io.netty.channel.nio.NioEventLoop.processSelectedKey(NioEventLoop.java:644)
        at io.netty.channel.nio.NioEventLoop.processSelectedKeysOptimized(NioEventLoop.java:579)
        at io.netty.channel.nio.NioEventLoop.processSelectedKeys(NioEventLoop.java:496)
        at io.netty.channel.nio.NioEventLoop.run(NioEventLoop.java:458)
        at io.netty.util.concurrent.SingleThreadEventExecutor$5.run(SingleThreadEventExecutor.java:858)
        at io.netty.util.concurrent.DefaultThreadFactory$DefaultRunnableDecorator.run(DefaultThreadFactory.java:138)
        at java.lang.Thread.run(Thread.java:745)
```