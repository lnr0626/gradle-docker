package com.lloydramey.gradle.docker.testing;

import lombok.Getter;
import lombok.Setter;
import org.gradle.api.DefaultTask;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.TaskAction;

import javax.inject.Inject;
import java.util.Collection;

@Getter
@Setter
public class SetupTestContainers extends DefaultTask {
    @Input
    private final ListProperty<ContainerDef> containers;
    @Input
    private final Property<String> networkName;
    @Internal
    private final DockerHelper client;

    @Inject
    public SetupTestContainers(ObjectFactory objects) {
        client = new DockerHelper();
        //noinspection unchecked
        containers = objects.listProperty(ContainerDef.class);
        networkName = objects.property(String.class);
    }

    @TaskAction
    public void setup() {
        Collection<ContainerDef> containers = this.containers.get();
        containers.forEach(client::pullImage);
        String network = this.networkName.get();

        client.destroyNetworkIfExists(network);

        client.createNetwork(network);
        containers.forEach(container -> {
            String id = client.runContainer(container, network);

            waitForHealthy(id);
        });
    }

    private void waitForHealthy(String id) {
        while(!client.isHealthy(id)) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
