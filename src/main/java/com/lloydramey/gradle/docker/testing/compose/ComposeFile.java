package com.lloydramey.gradle.docker.testing.compose;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lloydramey.gradle.docker.testing.ContainerDef;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComposeFile {
    private Map<String, Service> services = new HashMap<>();
    @Data
    public static class Service {
        private String image;
        private List<String> ports = new ArrayList<>();
        private List<String> volumes = new ArrayList<>();
        private Map<String, String> environment = new HashMap<>();
    }

    public Map<String, ContainerDef> toContainerDef() {
        Map<String, ContainerDef> definitions = new HashMap<>();

        for(Map.Entry<String, Service> entry : services.entrySet()) {
            Service service = entry.getValue();
            ContainerDef def = new ContainerDef(entry.getKey());
            def.setImage(service.getImage());

            for(String port : service.getPorts()) {
                String[] parts = port.split(":");
                if(parts.length == 2) {
                    def.getExposedPorts().put(parts[0], parts[1]);
                }
            }

            for(String volume : service.getVolumes()) {
                String[] parts = volume.split(":");
                if(parts.length >= 2) {
                    def.getBinds().put(parts[0], parts[1]);
                }
            }
            def.setEnv(service.getEnvironment());

            definitions.put(def.getName(), def);
        }

        return definitions;
    }
}
