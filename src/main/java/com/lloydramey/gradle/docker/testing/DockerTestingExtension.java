package com.lloydramey.gradle.docker.testing;

import groovy.lang.Closure;
import lombok.Data;
import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.Project;
import org.gradle.api.tasks.Input;

import java.io.File;

@Data
public class DockerTestingExtension {
    @Input
    private final NamedDomainObjectContainer<ContainerDef> containers;

    @Input
    private File composeFile;

    public DockerTestingExtension(Project project) {
        containers = project.container(ContainerDef.class);
        composeFile = null;
    }

    public NamedDomainObjectContainer<ContainerDef> containers(final Closure config) {
        return containers.configure(config);
    }
}
