package com.lloydramey.gradle.docker.testing;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.*;
import com.github.dockerjava.api.model.*;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.PullImageResultCallback;
import com.github.dockerjava.netty.NettyDockerCmdExecFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DockerHelper {
    private static final Logger logger = LoggerFactory.getLogger(DockerHelper.class);
    private static DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().build();
    private static DockerCmdExecFactory dockerCmdExecFactory = new NettyDockerCmdExecFactory();
    private static DockerClient client = DockerClientBuilder.getInstance(config).withDockerCmdExecFactory(dockerCmdExecFactory).build();

    private PullImageResultCallback pullCallback = new PullImageResultCallback() {
        @Override
        public void onNext(PullResponseItem item) {
            logger.debug("item = " + item);
            super.onNext(item);
        }
    };

    public String createNetwork(String networkName) {
        CreateNetworkResponse response = client.createNetworkCmd()
                .withName(networkName)
                .withCheckDuplicate(true)
                .withDriver("bridge")
                .withAttachable(true)
                .exec();

        String networkId = response.getId();

        logger.debug("Created bridge network with id " + networkId);

        Network network = client.inspectNetworkCmd()
                .withNetworkId(networkId)
                .exec();

        logger.debug("Created network: " + network);

        return networkId;
    }

    public String runContainer(ContainerDef container, String network) {
        logger.debug("Creating and starting " + container);
        CreateContainerCmd cmd = client.createContainerCmd(container.getImage())
                .withName(getContainerName(container, network))
                .withNetworkMode(network)
                .withAliases(container.getName());

        cmd.withEnv(container.getEnv().entrySet().stream()
                .map(entry -> String.format("%s=%s", entry.getKey(), entry.getValue()))
                .collect(Collectors.toList()));

        List<Volume> volumes = new ArrayList<>();
        List<Bind> binds = new ArrayList<>();

        container.getBinds().forEach((hostPath, containerPath) -> {
            Volume v = new Volume(containerPath);
            Bind bind = new Bind(hostPath, v);
            volumes.add(v);
            binds.add(bind);
        });

        cmd.withVolumes(volumes);
        cmd.withBinds(binds);

        List<ExposedPort> ports = new ArrayList<>();
        Ports bindings = new Ports();

        container.getExposedPorts().forEach((hostPort, containerPort) -> {
            ExposedPort port = ExposedPort.parse(containerPort);
            bindings.bind(port, Ports.Binding.parse(hostPort));
            ports.add(port);
        });

        cmd.withExposedPorts(ports);
        cmd.withPortBindings(bindings);
        cmd.withPublishAllPorts(container.isPublishAllPorts());

        if (!container.getEntryPoint().isEmpty()) {
            cmd.withEntrypoint(container.getEntryPoint());
        }

        if (!container.getCommand().isEmpty()) {
            cmd.withCmd(container.getCommand());
        }

        CreateContainerResponse response = cmd.exec();

        client.startContainerCmd(response.getId()).exec();

        return response.getId();
    }

    private String getContainerName(ContainerDef container, String network) {
        return String.format("tests_%s:%s", network, container.getName())
                .replaceAll("[^a-zA-Z0-9_.-]", "_");
    }

    public void destroyContainer(String id) {
        logger.debug("Destroying container " + id);
        client.removeContainerCmd(id).withForce(true).exec();
    }

    public void destroyNetwork(String networkId) {
        logger.debug("Deleting network " + networkId);
        client.removeNetworkCmd(networkId).exec();
    }

    public String findNetworkId(String networkName) {
        return client.inspectNetworkCmd()
                .withNetworkId(networkName)
                .exec()
                .getId();
    }

    public boolean isHealthy(String id) {
        return Optional.ofNullable(client.inspectContainerCmd(id)
                .exec()
                .getState())
                .flatMap(state -> Optional.ofNullable(state.getHealth()))
                .flatMap(health -> Optional.ofNullable(health.getStatus()))
                .map("healthy"::equalsIgnoreCase)
                .orElse(true);
    }

    public List<String> getExposedPorts(String id) {
        InspectContainerResponse response = client.inspectContainerCmd(id)
                .exec();

        return response.getNetworkSettings()
                .getPorts()
                .getBindings()
                .entrySet().stream()
                .flatMap(entry -> Stream.of(entry.getValue()))
                .map(Ports.Binding::getHostPortSpec)
                .collect(Collectors.toList());
    }

    public void pullImage(ContainerDef containerDef) {
        String image = containerDef.getImage();
        Identifier id = Identifier.fromCompoundString(image);
        logger.debug("id = {}", id);
        PullImageCmd cmd = client.pullImageCmd(id.repository.name);
        cmd.withTag(id.tag.or("latest"));
        cmd.exec(pullCallback).awaitSuccess();
    }

    public void destroyNetworkIfExists(String network) {
        getNetworkIfPresent(network).ifPresent(net -> {
            net.getContainers().forEach((name, container) -> destroyContainer(name));

            destroyNetwork(net.getId());
        });
    }

    private Optional<Network> getNetworkIfPresent(String network) {
        try {
            return Optional.of(client.inspectNetworkCmd()
                    .withNetworkId(network)
                    .exec());
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
