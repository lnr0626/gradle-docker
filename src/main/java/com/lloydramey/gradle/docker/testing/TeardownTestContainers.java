package com.lloydramey.gradle.docker.testing;

import lombok.Getter;
import lombok.Setter;
import org.gradle.api.DefaultTask;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.TaskAction;

@Getter
@Setter
public class TeardownTestContainers extends DefaultTask {
    @Input
    private final Property<String> network;
    @Internal
    private final DockerHelper client;

    public TeardownTestContainers() {
        client = new DockerHelper();
        network = getProject().getObjects().property(String.class);
    }

    @TaskAction
    public void teardown() {
        client.destroyNetworkIfExists(network.get());
    }
}
