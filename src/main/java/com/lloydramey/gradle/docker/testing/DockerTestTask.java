package com.lloydramey.gradle.docker.testing;

import lombok.Getter;
import lombok.Setter;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.invocation.Gradle;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.TaskAction;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public class DockerTestTask extends DefaultTask {

    private static final String MAPPED_CLASSPATH_ROOT = "/classpath";
    private static final String TEST_IMAGE = "openjdk:8";

    @Input
    private final ListProperty<ContainerDef> containers;

    @Internal
    private final DockerHelper client;

    @Input
    private final Property<Map<String, String>> testEnvironment;

    @Input
    private final ListProperty<String> testClassNames;

    @Inject
    public DockerTestTask(ObjectFactory objects) {
        client = new DockerHelper();

        containers = objects.listProperty(ContainerDef.class);
        //noinspection unchecked
        testEnvironment = (Property<Map<String, String>>) (Object) objects.property(Map.class);
        testClassNames = objects.listProperty(String.class);
    }

    @TaskAction
    public void executeTests() {
        // zeroth detect and cleanup leftover docker containers/network

//        // first create a network
//        String network = UUID.randomUUID().toString();
//        String networkId = client.createNetwork(network);
//
//        // second create containers and wait for them to be healthy
//        Collection<ContainerDef> containers = this.containers.get();
//
//        containers.forEach(client::pullImage);
//
//        containers
//                .forEach(container -> {
//                    try {
//                        client.pullImage(container);
//                    } catch(Exception e) {
//                        getProject().getLogger().error("Error while pulling image", e);
//                    }
//                });
//
//        List<String> ids = containers.stream()
//                .map(container -> client.runContainer(container, networkId))
//                .collect(Collectors.toList());
//
//        ids.forEach(this::waitForHealthy);

        // third start up testing container
        ContainerDef container = createTestContainerDefinition(getProject());
//        client.pullImage(container);

        System.out.println(container.getDockerRunCommand());


//        String containerId = client.runContainer(container, networkId);
//
//        // fourth parse test results
//
//        // fifth tear down containers
//        client.destroyContainer(containerId);
//        ids.forEach(client::destroyContainer);
//
//        // sixth destroy network
//        client.destroyNetwork(networkId);
    }

    private static Map<String, String> getMappedClasspathRoots(Set<Path> classpathRoots, String mappingRoot) {
        Map<String, String> mappings = new HashMap<>();

        for(Path root : classpathRoots) {
            String path = root.toString();
            String mapped = mapClasspathElement(path, mappingRoot);

            mappings.put(path, mapped);
        }

        return mappings;
    }

    private Set<File> getTestRuntimeClasspath(Project project) {
        JavaPluginConvention plugin = project.getConvention().getPlugin(JavaPluginConvention.class);
        FileCollection files = plugin.getSourceSets().getByName("test").getRuntimeClasspath();
        return files.getFiles();
    }

    private static Set<String> mapClasspath(final Set<File> classpath, final String mappingRoot) {
        return classpath.stream()
                .map(File::getAbsolutePath)
                .map(path -> mapClasspathElement(path, mappingRoot))
                .collect(Collectors.toSet());
    }

    private static String mapClasspathElement(String path, String mappingRoot) {
        return String.format("%s%s", mappingRoot, path.replaceAll("((?<!\\\\)\\\\|[A-Za-z]:\\\\)", "/"));
    }

    private ContainerDef createTestContainerDefinition(Project project) {
        ContainerDef container = new ContainerDef(UUID.randomUUID().toString());
        container.setImage(TEST_IMAGE);
        Set<File> runtimeClasspath = getTestRuntimeClasspath(project);
        Set<Path> classpathRoots = findClasspathRoots(runtimeClasspath, project.getGradle(), project);
        container.setBinds(getMappedClasspathRoots(classpathRoots, MAPPED_CLASSPATH_ROOT));
        container.setEnv(testEnvironment.getOrNull());
        container.setEntryPoint("java");


        Set<String> classpath = mapClasspath(runtimeClasspath, MAPPED_CLASSPATH_ROOT);
        container.addArgs("-cp", classpath.stream().collect(Collectors.joining(":")));
        container.addArgs(org.junit.runner.JUnitCore.class.getName());
        container.addArgs(testClassNames.get());
        return container;
    }

    static Set<Path> findClasspathRoots(Set<File> classpath, Gradle gradle, Project project) {
        Path gradleUserHome = gradle.getGradleUserHomeDir().toPath();
        Path projectDirectory = project.getProjectDir().toPath();

        Set<Path> roots = new HashSet<>();
        roots.add(gradleUserHome);
        roots.add(projectDirectory);

        for(File file : classpath) {
            if(roots.stream().noneMatch(root -> file.toPath().startsWith(root))) {
                roots.add(file.getParentFile().toPath());
            }
        }

        return roots;
    }

    private void waitForHealthy(String id) {
        while (!client.isHealthy(id)) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
