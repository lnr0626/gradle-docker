package com.lloydramey.gradle.docker.testing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.lloydramey.gradle.docker.testing.compose.ComposeFile;
import io.vavr.control.Either;
import io.vavr.control.Option;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.provider.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public class DockerTestingPlugin implements Plugin<Project> {

    public static final String EXTENSION_NAME = "dockerTesting";
    private static final Logger logger = LoggerFactory.getLogger(DockerTestingPlugin.class);

    @Override
    public void apply(Project project) {
        DockerTestingExtension ext = project.getExtensions().create(EXTENSION_NAME, DockerTestingExtension.class, project);
        Provider<List<ContainerDef>> containersProvider = project.provider(getContainerDefinitions(ext));
        String networkName = project.getPath();

        Task setup = project.getTasks().create("setupTestContainers", SetupTestContainers.class, task -> {
            task.getContainers().set(containersProvider);
            task.getNetworkName().set(networkName);
        });

        Task teardown = project.getTasks().create("teardownTestContainers", TeardownTestContainers.class, task -> {
            task.getNetwork().set(networkName);
        });

        Task test = project.getTasks().getByName("test");
        test.dependsOn(setup);
        setup.finalizedBy(teardown);
        teardown.mustRunAfter(test);

        Task customTestTask = project.getTasks().create("dockerTest", DockerTestTask.class, task -> {
            task.getContainers().set(containersProvider);
            task.getTestEnvironment().set(new HashMap<>());
            task.getTestClassNames().set(new ArrayList<>());
        });
    }

    private Callable<List<ContainerDef>> getContainerDefinitions(DockerTestingExtension ext) {
        return new CachedCallable<>(() -> {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            File file = ext.getComposeFile();
            logger.debug("ext.getComposeFile() = {}", file);
            if (file != null) {
                logger.debug("mapper.readValue(file, ComposeFile.class) = {}", mapper.readValue(file, ComposeFile.class));
            }
            Either<Throwable, Map<String, ContainerDef>> composeFileContents = Option.of(ext.getComposeFile()).toTry()
                    .mapTry(f -> mapper.readValue(f, ComposeFile.class))
                    .map(ComposeFile::toContainerDef)
                    .toEither();
            if (composeFileContents.isLeft()) {
                logger.debug("{}", composeFileContents.getLeft());
            }
            Map<String, ContainerDef> containers = composeFileContents.getOrElse(HashMap::new);

            ext.getContainers().getAsMap().forEach((name, def) -> {
                if (containers.containsKey(name)) {
                    containers.put(name, containers.get(name).overrideWith(def));
                } else {
                    containers.put(name, def);
                }
            });

            return new ArrayList<>(containers.values());
        });
    }

    private class CachedCallable<T> implements Callable<T> {
        private AtomicReference<T> value = new AtomicReference<>();
        private final Object sync = new Object();
        private final Callable<T> uncachedCall;

        CachedCallable(Callable<T> callable) {
            uncachedCall = callable;
        }

        @Override
        public T call() throws Exception {
            synchronized (sync) {
                T val = value.get();
                if (val == null) {
                    val = uncachedCall.call();
                    value.set(val);
                }
                return val;
            }
        }
    }
}
