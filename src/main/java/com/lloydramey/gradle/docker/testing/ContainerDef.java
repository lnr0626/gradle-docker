package com.lloydramey.gradle.docker.testing;

import io.vavr.control.Option;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class ContainerDef {
    private final String name;
    private String image;
    private Map<String, String> binds = new HashMap<>();
    private Map<String, String> exposedPorts = new HashMap<>();
    private Map<String, String> env = new HashMap<>();
    private boolean publishAllPorts = false;
    private List<String> entryPoint = new ArrayList<>();
    private List<String> command = new ArrayList<>();

    public void addArgs(List<String> args) {
        command.addAll(args);
    }

    public void addArgs(String... args) {
        command.addAll(Arrays.asList(args));
    }

    public void setEntryPoint(String... entryPoint) {
        this.entryPoint = Arrays.asList(entryPoint);
    }

    public ContainerDef overrideWith(ContainerDef other) {
        ContainerDef overridden = new ContainerDef(other.getName());
        overridden.setImage(Option.of(other.getImage()).getOrElse(getImage()));

        overridden.getEnv().putAll(env);
        other.getEnv().forEach(overridden.getEnv()::put);

        overridden.getBinds().putAll(binds);
        other.getBinds().forEach(overridden.getBinds()::put);

        overridden.getExposedPorts().putAll(exposedPorts);
        other.getExposedPorts().forEach(overridden.getExposedPorts()::put);

        overridden.setPublishAllPorts(other.isPublishAllPorts());

        return overridden;
    }

    public String getDockerRunCommand() {
        StringBuilder builder = new StringBuilder();

        builder.append("docker run --rm -it");

        if (!entryPoint.isEmpty()) {
            builder.append(" --entrypoint \"").append(String.join(" ", entryPoint)).append("\"");
        }

        if (!binds.isEmpty()) {
            builder.append(" ")
                    .append(binds.entrySet().stream()
                            .map(entry -> String.format("-v %s:%s", entry.getKey(), entry.getValue()))
                            .collect(Collectors.joining(" ")));
        }

        builder.append(" ").append(image);

        if (!command.isEmpty()) {
            builder.append(" ").append(String.join(" ", command));
        }

        return builder.toString();
    }
}
