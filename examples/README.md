These are a few example applications meant to demonstrate why this plugin exists. All of them are trivialized 
applications - the point isn't to showcase interesting applications, but to showcase methods for solving the problem,
and in particular their shortcomings.

The same type of problem applies to all of these examples. How do you test that your application correctly integrates with
an external resource/server/etc.? There are a few general solutions - first, you could just mock everything out and not
test anything. This is the easiest, but also gets you nowhere. Second, you could have all of your developers/testers
install a copy of the dependent services on their workstations, or point all of your tests to a 'test' instance of the
service. However, this will lead to tests failing due to misconfigured test environments (among other issues). Third,
you could rely on in memory implementations of all the external services. This could actually work for some things (for
instance, I know there is an in memory redis implementation that works well). However, not everything supports this,
and it can often lead to a large amount of code written just to support standing up and tearing down services for tests.

Another solution is to use docker to manage development environments. This works decently well, but lacks good automation.
I've included docker-compose files for each of the examples that demonstrate how this would work.

## db

This application is a (very) simple database backed application that stores information about camping equipment (name, 
category, description, and weight).

Usually you'll use an in-memory database (like h2) to test java applications with database backends, however that hides
issues with the mysql connector version (I actually experienced this while writing this example), and issues with the
slight variations present in the different sql dialects.

## s3

This application stores pictures in s3 and metadata for those pictures in dynamo. The main driver for not using real s3+
dynamo for testing here is to reduce cost, and to prevent network issues from failing tests (network issues can be 
simulated when you want to test failure conditions)

## messaging

This application listens for messages on sqs, and if the message is in json, converts it to xml. If it's in xml, it 
passes the message through, and if it's in an unknown format it fails processing and goes to a dead letter sqs queue.