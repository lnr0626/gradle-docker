package com.lloydramey.gradle.testing.examples.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DynamoTests {

    @LocalServerPort
    private Long port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private FileRepository repo;

    @Autowired
    private FileContentStore store;

    @Autowired
    private AmazonS3 s3;

    @Value("${spring.content.s3.bucket}")
    private String bucketName;

    private HttpClient client = HttpClientBuilder.create().build();

    /**
     * Make sure each test starts from a clean slate.
     */
    @After
    public void cleanUpEverything() {
        repo.findAll().forEach(store::unsetContent);
        repo.deleteAll();
    }

    @Test
    public void storeFileMetadataAndFetch() throws IOException {
        File request = newFile();

        File created = restTemplate.postForObject("/files", request, File.class);

        assertThat(created.getId(), notNullValue());
        assertThat(created.getCreated(), notNullValue());
        assertThat(created.getName(), is(request.getName()));
        assertThat(created.getSummary(), is(request.getSummary()));

        File[] files = restTemplate.getForObject("/files", File[].class);

        assertThat(files.length, is(1));
        assertThat(files[0], is(created));
    }

    @Test
    @Ignore("files/{id} is not working currently")
    public void getFileInfo() throws Exception {
        //setup
        File file = repo.save(newFile());

        File retrieved = restTemplate.getForObject("/files/" + file.getId(), File.class);
        assertThat(retrieved, is(file));
    }

    @Test
    public void setFileContent() throws Exception {
        //setup
        File file = repo.save(newFile());

        byte[] bytes = getContentBytes("static/app.js");
        File updated = putContentForFile(file, bytes);

        assertThat(updated.getContentId(), notNullValue());
        assertThat(updated.getMimeType(), is("application/octet-stream"));
        assertThat(updated.getContentLength(), is((long) bytes.length));

        byte[] fetched = getFileContent(file);

        assertThat(fetched, is(bytes));
    }

    @Test
    public void updateFileContent() throws Exception {
        File file = repo.save(newFile());
        byte[] original = getContentBytes("static/app.js");
        file.setMimeType("application/octet-stream");
        store.setContent(file, new ByteArrayInputStream(original));
        file = repo.save(file);

        byte[] updated = getContentBytes("static/index.html");
        File withNewContent = putContentForFile(file, updated);

        assertThat(withNewContent.getContentLength(), is((long) updated.length));

        byte[] fetched = getFileContent(withNewContent);
        assertThat(fetched, is(updated));
    }

    @Test
    public void deletingFilesWithContent() throws Exception {
        File file = repo.save(newFile());
        byte[] original = getContentBytes("static/app.js");
        file.setMimeType("application/octet-stream");
        store.setContent(file, new ByteArrayInputStream(original));
        file = repo.save(file);

        restTemplate.delete("/files/" + file.getId());

        assertThat(repo.findAll(), hasSize(0));
        assertThat(s3.listObjects(bucketName).getObjectSummaries(), hasSize(0));
    }

    @Test
    public void deletingFilesWithoutContent() throws Exception {
        File file = repo.save(newFile());

        restTemplate.delete("/files/" + file.getId());

        assertThat(repo.findAll(), hasSize(0));
        assertThat(s3.listObjects(bucketName).getObjectSummaries(), hasSize(0));
    }

    private File newFile() {
        File request = new File();
        request.setName("name");
        request.setSummary("summary");
        return request;
    }

    private byte[] getFileContent(File file) throws IOException {
        HttpGet get = new HttpGet("http://localhost:" + port + "/files/" + file.getId());
        get.setHeader("Content-Type", "application/octet-stream");
        HttpResponse response = client.execute(get);
        HttpEntity entity = response.getEntity();
        byte[] fetched = IOUtils.toByteArray(entity.getContent());
        entity.getContent().close();
        return fetched;
    }

    private File putContentForFile(File file, byte[] bytes) throws IOException {
        File saved;
        HttpPut put = new HttpPut("http://localhost:" + port + "/files/" + file.getId());
        put.setEntity(
                MultipartEntityBuilder.create()
                        .addPart("file", new ByteArrayBody(bytes, ContentType.APPLICATION_OCTET_STREAM, "app.js"))
                        .build()
        );

        HttpResponse response = client.execute(put);
        HttpEntity entity = response.getEntity();
        saved = mapper.readValue(entity.getContent(), File.class);
        System.out.println("saved = " + saved);

        entity.getContent().close();
        return saved;
    }

    private byte[] getContentBytes(String resource) throws IOException {
        return IOUtils.toByteArray(File.class.getClassLoader().getResourceAsStream(resource));
    }

}

