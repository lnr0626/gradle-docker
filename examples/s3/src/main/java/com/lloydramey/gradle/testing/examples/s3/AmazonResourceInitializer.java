package com.lloydramey.gradle.testing.examples.s3;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AmazonResourceInitializer {

    @Autowired
    AmazonDynamoDB dynamoDB;

    @Autowired
    AmazonS3 s3;

    @Value("${spring.content.s3.bucket}")
    String bucketName;

    @PostConstruct
    public void initialize() {
        if (!dynamoDB.listTables().getTableNames().contains("File")) {
            dynamoDB.createTable(new CreateTableRequest()
                    .withTableName("File")
                    .withProvisionedThroughput(new ProvisionedThroughput(5L, 5L))
                    .withAttributeDefinitions(
                            new AttributeDefinition().withAttributeName("id").withAttributeType("S")
                    )
                    .withKeySchema(new KeySchemaElement().withKeyType("HASH").withAttributeName("id"))
            );
        }

        if (!s3.listBuckets().stream().anyMatch(bucket -> bucket.getName().equals(bucketName))) {
            s3.createBucket(bucketName);
        }
    }
}
