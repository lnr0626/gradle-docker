package com.lloydramey.gradle.testing.examples.s3;

import org.springframework.content.commons.repository.ContentStore;

public interface FileContentStore extends ContentStore<File, String> {
}