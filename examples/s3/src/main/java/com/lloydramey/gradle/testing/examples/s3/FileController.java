package com.lloydramey.gradle.testing.examples.s3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileRepository filesRepo;
    @Autowired
    private FileContentStore contentStore;

    @GetMapping("/files")
    public Iterable<File> getFiles() {
        return filesRepo.findAll();
    }

    @PostMapping("/files")
    public File createFile(@RequestBody File file) {
        return filesRepo.save(file);
    }

    @GetMapping(value = "/files/{id}", headers = "content-type=application/json")
    public File getFile(@PathVariable("id") String id) {

        logger.warn("id = " + id);
        return filesRepo.findOne(id);
    }

    @DeleteMapping("/files/{id}")
    public void delete(@PathVariable("id") String id) {
        logger.warn("id = " + id);
        File f = filesRepo.findOne(id);
        contentStore.unsetContent(f);
        filesRepo.delete(f);
    }

    @PutMapping(value = "/files/{fileId}")
    public ResponseEntity<File> setContent(@PathVariable("fileId") String id, @RequestParam("file") MultipartFile file)
            throws IOException {

        File f = filesRepo.findOne(id);
        f.setMimeType(file.getContentType());
        logger.info("file: {}", f);

        contentStore.setContent(f, file.getInputStream());

        // save updated content-related info
        File saved = filesRepo.save(f);
        logger.info("saved: {}", saved);

        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @GetMapping(value = "/files/{fileId}", headers = "accept!=application/json")
    public ResponseEntity<?> getContent(@PathVariable("fileId") String id) {

        File f = filesRepo.findOne(id);
        InputStreamResource inputStreamResource = new InputStreamResource(contentStore.getContent(f));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(f.getContentLength());
        headers.set("Content-Type", f.getMimeType());
        return new ResponseEntity<Object>(inputStreamResource, headers, HttpStatus.OK);
    }
}