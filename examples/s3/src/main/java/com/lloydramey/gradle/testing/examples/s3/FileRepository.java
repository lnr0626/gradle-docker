package com.lloydramey.gradle.testing.examples.s3;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface FileRepository extends CrudRepository<File, String> {
    List<File> findAll();
}
