package com.lloydramey.gradle.testing.examples.db

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "equipment", path = "equipment")
interface EquipmentRepository : PagingAndSortingRepository<CampingEquipment, Long>