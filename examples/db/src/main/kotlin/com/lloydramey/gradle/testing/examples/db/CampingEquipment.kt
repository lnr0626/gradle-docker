package com.lloydramey.gradle.testing.examples.db

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity(name = "equipment")
class CampingEquipment(
    var name: String = "",
    var category: String = "",
    var description: String = "",
    var weight: Double = 0.0,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long = 0
) {
    override fun equals(other: Any?): Boolean {
        return EqualsBuilder.reflectionEquals(this, other)
    }

    override fun toString(): String {
        return ToStringBuilder.reflectionToString(this)
    }

    override fun hashCode(): Int {
        return HashCodeBuilder.reflectionHashCode(this)
    }
}