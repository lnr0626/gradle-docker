package com.lloydramey.gradle.testing.examples.db

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DbApplication

fun main(args: Array<String>) {
    SpringApplication.run(DbApplication::class.java, *args)
}
