package com.lloydramey.gradle.testing.examples.db

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasSize
import org.hamcrest.collection.IsEmptyCollection.empty
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DbApplicationTests {

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @Autowired
    lateinit var repo: EquipmentRepository

    @After
    fun `clean everything up`() {
        repo.deleteAll()
    }

    @Test
    fun `create new equipment`() {
        val equipment = CampingEquipment("name", "category", "description", 34.2)
        val postResp = restTemplate.postForObject("/equipment", equipment, CampingEquipment::class.java)

        assertThat(postResp.category, `is`(equipment.category))
        assertThat(postResp.name, `is`(equipment.name))
        assertThat(postResp.description, `is`(equipment.description))
        assertThat(postResp.weight, `is`(equipment.weight))
    }

    @Test
    fun `fetch existing equipment`() {
        val equipment = repo.save(CampingEquipment("name", "category", "description", 34.2))

        val response = restTemplate.getForObject("/equipment/${equipment.id}", CampingEquipment::class.java)
        // the id isn't being set for some reason
        response.id = equipment.id

        assertThat(response, `is`(equipment))
    }

    @Test
    fun `update equipment`() {
        val equipment = repo.save(CampingEquipment("name", "category", "description", 34.2))

        val updated = CampingEquipment("new name", "category", "description", 40.0, equipment.id)

        restTemplate.put("/equipment/${equipment.id}", updated)

        val response = restTemplate.getForObject("/equipment/${equipment.id}", CampingEquipment::class.java)
        // the id isn't being set for some reason
        response.id = equipment.id

        assertThat(response, `is`(updated))
    }

    @Test
    fun `delete equipment`() {
        val equipment = repo.save(CampingEquipment("name", "category", "description", 34.2))

        restTemplate.delete("/equipment/${equipment.id}")

        val afterDelete = restTemplate.getForObject("/equipment", ResponseList::class.java)
        assertThat(afterDelete._embedded.equipment, empty())

        assertThat(repo.findAll().toList(), hasSize(0))
    }

}

data class EmbeddedList(val equipment: List<CampingEquipment>)
data class ResponseList(val _embedded: EmbeddedList, val _links: Links)
data class Links(val self: Self)
data class Self(val href: String)
