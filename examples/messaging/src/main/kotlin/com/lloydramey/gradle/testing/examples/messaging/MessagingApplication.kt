package com.lloydramey.gradle.testing.examples.messaging

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class MessagingApplication

fun main(args: Array<String>) {
    SpringApplication.run(MessagingApplication::class.java, *args)
}