package com.lloydramey.gradle.testing.examples.messaging

import org.apache.camel.builder.RouteBuilder
import org.springframework.stereotype.Component

@Component
class MessagingRouteBuilder : RouteBuilder() {
    override fun configure() {
        from("aws-sqs://{{sqs.queue}}?amazonSQSClient=AmazonSqs")
            .to("aws-sns://{{sns.topic}}?amazonSNSClient=amazonSns")
    }

}